package com.imaisnaini.bnipromoapp

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.core.view.MenuProvider
import androidx.core.view.WindowCompat
import com.imaisnaini.bnipromoapp.ui.BNIPromoApp
import com.imaisnaini.bnipromoapp.ui.PromosViewModel
import com.imaisnaini.bnipromoapp.ui.theme.BNIPromoAppTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val viewModel: PromosViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Displaying edge-to-edge
        WindowCompat.setDecorFitsSystemWindows(window, true)

        setContentView(ComposeView(this).apply {
            // Provide a different ComposeView instead of using ComponentActivity's setContent
            // method so that the `consumeWindowInsets` property can be set to `false`. This is
            // needed so that insets can be properly applied to `HomeScreen` which uses View interop
            // This can likely be changed when `HomeScreen` is fully in Compose.
            consumeWindowInsets = true
            setContent {
                BNIPromoAppTheme {
                    BNIPromoApp(
                        promosViewModel = viewModel,
                    )
                }
            }
        })
    }
}