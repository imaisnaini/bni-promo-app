package com.imaisnaini.bnipromoapp.ui

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imaisnaini.bnipromoapp.Promo
import com.imaisnaini.bnipromoapp.data.endpoint.ApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PromosViewModel @Inject constructor() : ViewModel() {
    var promosLiveData = MutableLiveData<List<Promo>>()
    fun getPromoLiveData() = promosLiveData
    var promosResponse: List<Promo> by mutableStateOf(listOf())

    private var errorMessage = MutableLiveData("")
    fun getErrorMessage() = errorMessage

    private var showLoading = MutableLiveData<Boolean>(false)
    fun getShowLoading() = showLoading

    private var detailLiveData = MutableLiveData<Promo>()
    fun getDetailLiveData() = detailLiveData

    fun findByID(idx: String){
        showLoading.value = true
        if (!promosLiveData.value.isNullOrEmpty()){
            val list = promosLiveData.value
            detailLiveData.value = list?.get(idx.toInt())
            showLoading.value = false
        }
//        viewModelScope.launch {
//            val apiService = ApiService.getInstance()
//            try {
//                val promo = apiService.getById(id)
//                detailLiveData.value =promo
//                errorMessage.value = null
//            }catch (e: Exception){
//                errorMessage.value = e.message
//                showLoading.value = false
//            }finally {
//                showLoading.value = false
//            }
//        }
    }
    fun findAll(){
        showLoading.value = true
        viewModelScope.launch {
            val apiService = ApiService.getInstance()
            try {
                val promoList = apiService.getAll()
                promosResponse = promoList
                if (!promoList.isEmpty()) {
                    promosLiveData.value = promoList
                }
                errorMessage.value = null
            }catch (e: Exception){
                errorMessage.value = e.message
                showLoading.value = false
            }finally {
                showLoading.value = false
            }
        }
    }
}