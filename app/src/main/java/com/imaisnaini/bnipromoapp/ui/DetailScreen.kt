package com.imaisnaini.bnipromoapp.ui.detail

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChevronLeft
import androidx.compose.material.icons.filled.GpsFixed
import androidx.compose.material.icons.filled.PinDrop
import androidx.compose.material.icons.filled.Sync
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidViewBinding
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import coil.compose.AsyncImageContent
import coil.compose.AsyncImagePainter
import coil.request.ImageRequest
import com.imaisnaini.bnipromoapp.Promo
import com.imaisnaini.bnipromoapp.databinding.HomeScreenBinding
import com.imaisnaini.bnipromoapp.ui.PromosViewModel
import com.imaisnaini.bnipromoapp.ui.home.PromoItem
import com.imaisnaini.bnipromoapp.ui.theme.*

@Composable
fun DetailScreen(
    promoId: String,
    onBackClick: () -> Unit = {},
    onAttached: (Toolbar) -> Unit = {},
    promosViewModel: PromosViewModel = hiltViewModel()
){
    val activity = (LocalContext.current as AppCompatActivity)
    promosViewModel.findByID(promoId)
    var promo = remember {
        mutableStateOf<Promo?>(null)
    }
    promosViewModel.getDetailLiveData().observe(activity){
        if (it != null){
            promo.value = it
        }
    }
    promosViewModel.getErrorMessage().observe(activity){
        if (!it.isNullOrEmpty()){
            Toast.makeText(activity, it, Toast.LENGTH_SHORT).show()
        }
    }

    AndroidViewBinding(factory = HomeScreenBinding::inflate) {
//        onAttached(toolbar)
//        activity.setSupportActionBar(toolbar)
        composeView.setContent {
            if (promosViewModel.getShowLoading().observeAsState().value == true){
                Column(verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier.fillMaxSize()) {
                    LinearProgressIndicator()
                    Text(text = "Loading Data, Please Wait..", color = Grey_700)
                }
            }else{
                val data = promo.value
                val img = data?.img!!.formats
                Column {
                    TopAppBar(onBackClick = onBackClick)
                    Column(modifier = Modifier
                        .fillMaxSize()
                        .background(
                            brush = Brush.verticalGradient(
                                colors = listOf(Color.Transparent, Turquoise_300)
                            )
                        )) {
                        Box(modifier = Modifier.fillMaxWidth()){
                            AsyncImage(
                                model = ImageRequest.Builder(LocalContext.current)
                                    .data("${img!!.medium!!.url}")
                                    .crossfade(true)
                                    .build(),
                                contentDescription = "Translated description of what the image contains",
                                contentScale = ContentScale.FillWidth,
                                modifier = Modifier.fillMaxWidth()
                            ) { state ->
                                if (state is AsyncImagePainter.State.Loading) {
                                    CircularProgressIndicator()
                                } else {
                                    AsyncImageContent() // Draws the image.
                                }
                            }
                        }
                        Column(modifier = Modifier
                            .fillMaxSize()
                            .padding(all = padding_normal)) {
                            Text("${data.nama}", style = Typography.h6,
                                modifier = Modifier.padding(vertical = padding_small))
                            Row() {
                                Icon(Icons.Filled.PinDrop, contentDescription = "Icon Location")
                                Text("${data.lokasi}", style = Typography.subtitle1,
                                    modifier = Modifier.padding(vertical = padding_small))
                            }
                            Text("${data.desc}")
                        }
                    }
                }
            }
        }
    }

}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun TopAppBar(
    onBackClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    TopAppBar(
        title = {
            Text(
                text = "BNI Promo App"
            )
        },
        navigationIcon = {
            IconButton(onClick = onBackClick) {
                Icon(imageVector = Icons.Filled.ChevronLeft, "Icon Button Back")
            }
        },
        elevation = 0.dp
    )
}