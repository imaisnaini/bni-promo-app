package com.imaisnaini.bnipromoapp.ui

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.widget.Toolbar
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavHostController
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.imaisnaini.bnipromoapp.ui.detail.DetailScreen
import com.imaisnaini.bnipromoapp.ui.home.HomeScreen

@Composable
fun BNIPromoApp(
    onAttached: (Toolbar) -> Unit = {},
    promosViewModel: PromosViewModel = hiltViewModel()
) {
    val navController = rememberNavController()
    BNIPromoNavHost(navHostController = navController, onAttached, promosViewModel)
}

@Composable
fun BNIPromoNavHost(
    navHostController: NavHostController,
    onAttached: (Toolbar) -> Unit = {},
    promosViewModel: PromosViewModel = hiltViewModel()
){
    val activity = (LocalContext.current as Activity)
    NavHost(navController = navHostController, startDestination = "home") {
        composable("home") {
            HomeScreen(
                onPromoClick = {
                    navHostController.navigate("details/${it}")
                },
                onAttached,
                promosViewModel
            )
        }
        composable(
            "details/{index}",
            arguments = listOf(navArgument("index") {
                type = NavType.StringType
            })
        ) {
            DetailScreen(
                it.arguments!!.getString("index", "0"),
                onBackClick = {
                    navHostController.navigateUp()
//                    promosViewModel.findAll()
                              },
                onAttached,
                promosViewModel
            )
        }
    }
}