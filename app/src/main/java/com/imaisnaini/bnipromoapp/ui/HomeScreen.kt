package com.imaisnaini.bnipromoapp.ui.home

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Sync
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidViewBinding
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import coil.compose.AsyncImageContent
import coil.compose.AsyncImagePainter
import coil.request.ImageRequest
import com.imaisnaini.bnipromoapp.Promo
import com.imaisnaini.bnipromoapp.databinding.HomeScreenBinding
import com.imaisnaini.bnipromoapp.ui.PromosViewModel
import com.imaisnaini.bnipromoapp.ui.theme.*

@Composable
fun HomeScreen(
    onPromoClick: (Int) -> Unit,
    onAttached: (Toolbar) -> Unit = {},
    promosViewModel: PromosViewModel = hiltViewModel()
) {
    val activity = (LocalContext.current as AppCompatActivity)
    promosViewModel.findAll()
    AndroidViewBinding(factory = HomeScreenBinding::inflate) {
//        onAttached(toolbar)
//        activity.setSupportActionBar(toolbar)
        composeView.setContent {
            val promoList = remember {
                mutableStateListOf<Promo>()
            }
            promosViewModel.getPromoLiveData().observe(activity){
                if (!it.isNullOrEmpty()){
                    promoList.apply {
                        clear()
                        addAll(it)
                    }
//                    Toast.makeText(activity, it.size.toString(), Toast.LENGTH_SHORT).show()
                }
            }
            promosViewModel.getErrorMessage().observe(activity){
                if (!it.isNullOrEmpty()){
                    Toast.makeText(activity, it, Toast.LENGTH_SHORT).show()
                }
            }

//            if (promoList.isEmpty()){
//                promosViewModel.findAll()
//            }
            Column(modifier = Modifier.fillMaxSize()) {
                HomeTopAppBar(onSyncClick = { promosViewModel.findAll() })
                if (promosViewModel.getShowLoading().observeAsState().value == true){
                    Column(verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier.fillMaxSize()) {
                        LinearProgressIndicator()
                        Text(text = "Loading Data, Please Wait..", color = Grey_700)
                    }
                }
                LazyColumn(){
                    itemsIndexed(promoList){index, item ->
                        PromoItem(data = item, onPromoClick, index)
                    }
                }
            }
        }
    }
}

@Composable
fun PromoItem(data: Promo, onPromoClick: (Int) -> Unit, index: Int){
    val img = data.img!!.formats
    Card(elevation = elevation_normal,
        modifier = Modifier
            .fillMaxWidth()
            .padding(all = padding_small)
            .clickable { onPromoClick(index) }
    ) {
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .data("${img!!.small!!.url}")
                .crossfade(true)
                .build(),
            contentDescription = "Translated description of what the image contains",
            contentScale = ContentScale.FillBounds
        ) { state ->
            if (state is AsyncImagePainter.State.Loading) {
                CircularProgressIndicator()
            } else {
                AsyncImageContent() // Draws the image.
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun HomeTopAppBar(
    onSyncClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    TopAppBar(
        title = {
            Text(
                text = "BNI Promo App"
            )
        },
        actions = {
            IconButton(onClick = onSyncClick) {
                Icon(imageVector = Icons.Filled.Sync, "Icon Button Sync")
            }
        },
        elevation = 0.dp
    )
}
