package com.imaisnaini.bnipromoapp.data.entity

import com.imaisnaini.bnipromoapp.Promo

data class PromoResponse(val promos: List<Promo>)
