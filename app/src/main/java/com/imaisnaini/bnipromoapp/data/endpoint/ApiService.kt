package com.imaisnaini.bnipromoapp.data.endpoint

import com.imaisnaini.bnipromoapp.BuildConfig
import com.imaisnaini.bnipromoapp.Promo
import com.imaisnaini.bnipromoapp.data.entity.PromoResponse
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("promos")
    suspend fun getAll(): List<Promo>

    @GET("promos/{id}")
    suspend fun getById(
        @Path("id") id: String
    ): Promo

    companion object{
        var apiService: ApiService? = null
        fun getInstance() : ApiService {
            if (apiService == null) {
                apiService = Retrofit.Builder()
                    .baseUrl(BuildConfig.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(ApiService::class.java)
            }
            return apiService!!
        }
    }
}