package com.imaisnaini.bnipromoapp

import com.google.gson.annotations.SerializedName


data class Thumbnail (

  @SerializedName("ext"    ) var ext    : String? = null,
  @SerializedName("url"    ) var url    : String? = null,
  @SerializedName("hash"   ) var hash   : String? = null,
  @SerializedName("mime"   ) var mime   : String? = null,
  @SerializedName("name"   ) var name   : String? = null,
  @SerializedName("path"   ) var path   : String? = null,
  @SerializedName("size"   ) var size   : Double? = null,
  @SerializedName("width"  ) var width  : Int?    = null,
  @SerializedName("height" ) var height : Int?    = null

)