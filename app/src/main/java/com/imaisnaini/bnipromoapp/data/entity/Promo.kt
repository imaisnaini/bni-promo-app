package com.imaisnaini.bnipromoapp

import com.google.gson.annotations.SerializedName


data class Promo (

  @SerializedName("id"           ) var id          : Int?    = null,
//  @SerializedName("Title"        ) var Title       : String? = null,
  @SerializedName("published_at" ) var publishedAt : String? = null,
  @SerializedName("created_at"   ) var createdAt   : String? = null,
  @SerializedName("updated_at"   ) var updatedAt   : String? = null,
//  @SerializedName("name_promo"   ) var namePromo   : String? = null,
//  @SerializedName("desc_promo"   ) var descPromo   : String? = null,
  @SerializedName("nama"         ) var nama        : String? = null,
  @SerializedName("desc"         ) var desc        : String? = null,
  @SerializedName("latitude"     ) var latitude    : String? = null,
  @SerializedName("longitude"    ) var longitude   : String? = null,
  @SerializedName("alt"          ) var alt         : String? = null,
  //@SerializedName("createdAt"    ) var createdAt   : String? = null,
  @SerializedName("lokasi"       ) var lokasi      : String? = null,
  @SerializedName("count"        ) var count       : Int?    = null,
  @SerializedName("img"          ) var img: Img? = Img()

)