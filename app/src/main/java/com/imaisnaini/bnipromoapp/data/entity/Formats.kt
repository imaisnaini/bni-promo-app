package com.imaisnaini.bnipromoapp

import com.google.gson.annotations.SerializedName


data class Formats (

  @SerializedName("small"     ) var small     : Small?     = Small(),
  @SerializedName("medium"    ) var medium    : Medium?    = Medium(),
  @SerializedName("thumbnail" ) var thumbnail : Thumbnail? = Thumbnail()

)